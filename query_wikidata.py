#!/usr/bin/python3
#limitado a queries con ?item e ?itemlabel :(
import sys
import os
import pywikibot
from pywikibot import pagegenerators as pg

FILE = sys.argv[1]
if os.path.exists(FILE) == False:
    #No existe el rq
    print('Failed to open {0}. Existe?'.format(FILE))
    sys.exit(1)

with open(FILE, 'r') as query_file:
    QUERY = query_file.read()

wikidata_site = pywikibot.Site("wikidata", "wikidata")
generator = pg.WikidataSPARQLPageGenerator(QUERY, site=wikidata_site)
print(generator)
#for item in generator:
#    print(item)